import React from "react";
// import ReactDOM from 'react-dom';
import App from "./App";
import { shallow } from "enzyme";

let wrapped = null;

describe("Component <App />", () => {
  const prepareComponent = (props = {}) => {
    wrapped = shallow(<App {...props} />);
    return wrapped;
  };

  it("should render without crashing", () => {
    wrapped = prepareComponent();
    expect(wrapped.find(".test-carousel").length).toBe(1);
  });
});
