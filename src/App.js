import React, { useEffect, useState } from "react";
import axios from "axios";
import { Container, Col, Row, Carousel } from "react-bootstrap";

import PoundImage from "./assets/GBP.jpg";
import FrankImage from "./assets/CHF.jpg";
import DollarImage from "./assets/USD.jpg";

import "./bootstrap.min.css";

function App() {
  const API_URL = `https://api.exchangeratesapi.io/latest`;
  const IMAGES = [PoundImage, FrankImage, DollarImage];

  const [currencies, setCurrency] = useState([]);
  const fetchCurrencies = async (base, currencies = []) => {
    const symbols = currencies.toString();
    const result = await axios.get(API_URL, {
      params: { base, symbols }
    });

    return setCurrency(result.data);
  };

  useEffect(() => {
    const API_URL = `https://api.exchangeratesapi.io/latest`;
    // would normally put this at the top of the function
    // moved here so as not tot trigger react-hooks/exhaustive-deps

    // double defining function for ease of reading
    // could have been an IIFE
    // also, this is so as not to break Rules of Hooks (react-hooks/exhaustive-deps)
    const fetchCurrencies = async (base, currencies = []) => {
      const symbols = currencies.toString();
      const result = await axios.get(API_URL, {
        params: { base, symbols }
      });

      return setCurrency(result.data);
    };

    fetchCurrencies("GBP", ["EUR"]);
  }, []); // componentDidMount, set currencies to GBP and EUR

  const onSelect = eventKey => {
    switch (eventKey) {
      case 1:
        return fetchCurrencies("CHF", ["USD"]);
      case 2:
        return fetchCurrencies("USD", ["GBP"]);
      default:
        return fetchCurrencies("GBP", ["EUR"]);
    }
  };

  const getCurrencyRatesAsString = (ratesObj = {}) => {
    for (let [key, value] of Object.entries(ratesObj)) {
      return `${key}: ${value}`;
    }
  };

  return (
    <Container className="h-100 w-100">
      <Row>
        <Col>
          <Carousel
            controls={false}
            onSelect={eventKey => onSelect(eventKey)}
            className="test-carousel"
          >
            {IMAGES.map((background, i) => (
              <Carousel.Item key={i}>
                <img className="d-block" src={background} alt="" />
                <Carousel.Caption>
                  {currencies.base} to{" "}
                  {getCurrencyRatesAsString(currencies.rates)}
                </Carousel.Caption>
              </Carousel.Item>
            ))}
          </Carousel>
        </Col>
      </Row>
    </Container>
  );
}

export default App;
